#!/bin/bash

mkdir -p deps
cd deps

git clone https://gitlab.freedesktop.org/xrdesktop/libinputsynth.git
cd libinputsynth
git checkout $1
meson build --prefix /usr
ninja -C build install
cd ../..
